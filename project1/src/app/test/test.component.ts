import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  template: `
    <p>
      Welcome Test Component

    </p>
    <input [(ngModel)]="name" type="text">

    <br>
    {{name}}

  `,
  styles: [
   
  ]
})
export class TestComponent implements OnInit {
  
  public name = "";
  
  constructor() { }

  ngOnInit() {
    
  }

}
