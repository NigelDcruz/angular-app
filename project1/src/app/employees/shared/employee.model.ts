export class Employee {
    $key: string;
    name: String;
    position: string;
    office: string;
    salary: number;
}
